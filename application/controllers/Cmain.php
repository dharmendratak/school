<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cmain extends CI_Controller {

	public function __construct()
    {
        // call CodeIgniter's default Constructor
        parent::__construct();
        $this->load->library('session');
		$this->load->database();
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcomeform');
	}

	public function personal(){
		if(isset($_POST['submit'])){
			$this->form_validation->set_rules('usertype', 'User Type', 'trim|required');
			$this->form_validation->set_rules('gender', 'Gender', 'trim|required');
			$this->form_validation->set_rules('treatment', 'Being treated for', 'trim|required');
			$this->form_validation->set_rules('fullname', 'Full Name', 'trim|required');
			if($_POST['dob'] == ''){
				$this->form_validation->set_rules('dob','Date of Birth', 'trim|required');
			}else{
				$this->form_validation->set_rules('dob','Date of Birth', array('regex_match[/^((0[1-9]|[12][0-9]|3[01])[- \/.](0[1-9]|1[012])[- \/.](19|20)\d\d)$/]'));
				// $this->form_validation->set_message(array('regex_match[/^((0[1-9]|[12][0-9]|3[01])[- \/.](0[1-9]|1[012])[- \/.](19|20)\d\d)$/]'), 'Date of birth is not in correct format.');
			}
			// $this->form_validation->set_rules('dob', 'Date of Birth', 'trim|required');

			if ($this->form_validation->run() == FALSE){
				$this->load->view('welcomeform');
			}else{
				$usertype= $_POST['usertype'];
				$gender = $_POST['gender'];
				$treatment = $_POST['treatment'];
				$fullname = $_POST['fullname'];
				$dob = $_POST['dob'];
				$data['userdata'] = array(
					'usertype' => $usertype,
					'gender' => $gender,
					'treatment' => $treatment,
					'fullname' => $fullname,
					'dob' => $dob
				);
				$this->load->view('personal', $data);
			}
		}
	}

	public function medical(){
		if(isset($_POST['submit'])){
			$this->form_validation->set_rules('fname', 'Father Name', 'trim|required');
			$this->form_validation->set_rules('mname', 'Mother Name', 'trim|required');
			$this->form_validation->set_rules('contactno', 'Contace Number', 'trim|required|regex_match[/^[0-9]{10}$/]');
			$this->form_validation->set_rules('address', 'Address', 'trim|required');

			if ($this->form_validation->run() == FALSE){
				$data['userdata'] = array(
					'usertype' => $_POST['usertype'],
					'gender' => $_POST['gender'],
					'treatment' => $_POST['treatment'],
					'fullname' => $_POST['fullname'],
					'dob' => $_POST['dob']
				);
				$this->load->view('personal', $data);
			}else{
				$data['userdata'] = array(
					'usertype' => $_POST['usertype'],
					'gender' => $_POST['gender'],
					'treatment' => $_POST['treatment'],
					'fullname' => $_POST['fullname'],
					'dob' => $_POST['dob'],
					'fname' => $_POST['fname'],
					'mname' => $_POST['mname'],
					'contactno' => $_POST['contactno'],
					'address' => $_POST['address']
				);
				$this->load->view('medical', $data);
			}
		}
	}

	public function register(){
		if(isset($_POST['register'])){
			$this->form_validation->set_rules('medicine', 'Medicine', 'trim|required');
			$this->form_validation->set_rules('doctor', 'Current Doctor', 'trim|required');
			$this->form_validation->set_rules('city', 'City', 'trim|required');

			if ($this->form_validation->run() == FALSE){
				$data['userdata'] = array(
					'usertype' => $_POST['usertype'],
					'gender' => $_POST['gender'],
					'treatment' => $_POST['treatment'],
					'fullname' => $_POST['fullname'],
					'dob' => $_POST['dob'],
					'fname' => $_POST['fname'],
					'mname' => $_POST['mname'],
					'contactno' => $_POST['contactno'],
					'address' => $_POST['address']
				);
				$this->load->view('academic', $data);
			}else{
				$data['userdata'] = array(
					'usertype' => $_POST['usertype'],
					'gender' => $_POST['gender'],
					'treatment' => $_POST['treatment'],
					'fullname' => $_POST['fullname'],
					'dob' => $_POST['dob'],
					'fname' => $_POST['fname'],
					'mname' => $_POST['mname'],
					'contactno' => $_POST['contactno'],
					'address' => $_POST['address'],
					'medicine' => $_POST['medicine'],
					'doctor' => $_POST['doctor'],
					'city' => $_POST['city']
				);

				$this->load->view('registrationform', $data);
			}
		}
	}
}
