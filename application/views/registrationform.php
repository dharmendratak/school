<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Welcome to CodeIgniter</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <style type="text/css">
        .emptyrow{
            height: 20px;
            width: 100%;
        }
        .datepicker td, .datepicker th {
            width: 2em;
            height: 2em;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-12 text-center">
            <h1>Registration Details</h1>
            <h3>User has been registered with following details:</h3>
        </div>
    </div>
    <div class="emptyrow"></div>
    <div class="row">
        <div class="col-2"></div>
        <div class="col-8">
            <div class="row">
                <div class="col-4 text-right">
                    Full Name: 
                </div>
                <div class="col-8">
                    <?= $userdata['fullname'] ?>
                </div>
            </div>
            <div class="emptyrow"></div>
            <div class="row">
                <div class="col-4 text-right">
                    User Type:
                </div>
                <div class="col-8">
                    <?= $userdata['usertype'] ?>
                </div>
            </div>
            <div class="emptyrow"></div>
            <div class="row">
                <div class="col-4 text-right">
                Being treated for 
                </div>
                <div class="col-8">
                    <?= $userdata['treatment'] ?>
                </div>
            </div>
            <div class="emptyrow"></div>
            <div class="row">
                <div class="col-4 text-right">
                    Gender: 
                </div>
                <div class="col-8">
                    <?= $userdata['gender'] ?>
                </div>
            </div>
            <div class="emptyrow"></div>
            <div class="row">
                <div class="col-4 text-right">
                    Date of birth: 
                </div>
                <div class="col-8">
                    <?= $userdata['dob'] ?>
                </div>
            </div>
            <div class="emptyrow"></div>
            <div class="row">
                <div class="col-4 text-right">
                    Father Name: 
                </div>
                <div class="col-8">
                    <?= $userdata['fname'] ?>
                </div>
            </div>
            <div class="emptyrow"></div>
            <div class="row">
                <div class="col-4 text-right">
                    Mother Name: 
                </div>
                <div class="col-8">
                    <?= $userdata['mname'] ?>
                </div>
            </div>
            <div class="emptyrow"></div>
            <div class="row">
                <div class="col-4 text-right">
                    Contact Number: 
                </div>
                <div class="col-8">
                    <?= $userdata['contactno'] ?>
                </div>
            </div>
            <div class="emptyrow"></div>
            <div class="row">
                <div class="col-4 text-right">
                    Address: 
                </div>
                <div class="col-8">
                    <?= $userdata['address'] ?>
                </div>
            </div>
            <div class="emptyrow"></div>
            <div class="row">
                <div class="col-4 text-right">
                Medicine or product interested in: 
                </div>
                <div class="col-8">
                    <?= $userdata['medicine'] ?>
                </div>
            </div>
            <div class="emptyrow"></div>
            <div class="row">
                <div class="col-4 text-right">
                Current Doctor Name: 
                </div>
                <div class="col-8">
                    <?= $userdata['doctor'] ?>
                </div>
            </div>
            <div class="emptyrow"></div>
            <div class="row">
                <div class="col-4 text-right">
                    City: 
                </div>
                <div class="col-8">
                    <?= $userdata['city'] ?>
                </div>
            </div>
            <div class="emptyrow"></div>
        </div>
    </div>
</div>
</body>
</html>